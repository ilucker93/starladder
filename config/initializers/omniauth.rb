Rails.application.config.middleware.use OmniAuth::Builder do
  provider :starladder,
           Rails.application.secrets.starladder_oauth[:key],
           Rails.application.secrets.starladder_oauth[:secret],
           # TODO: change for deployed version
           { callback_url: 'http://127.0.0.1:3000/auth/starladder/callback' }
end